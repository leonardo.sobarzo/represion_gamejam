extends Node

#don't know if this is faster than $World
onready var world = get_node("/root/Main/World")
var stage: int
var stageArray: Array
var player: KinematicBody
var countPropaganda: int

func _ready():
	stage = 0
	stageArray =["res://scenes/Escena_1.tscn", "res://scenes/Escena_2.tscn",
	"res://scenes/Escena_3.tscn"]
	

func changeStage():
	#first time
	var scene = load(stageArray[stage])
	if world.get_child_count() > 0:
		#Testear
		world.get_child(0).queue_free()
	
	world.add_child(scene.instance())
	stage += 1
	player = get_node("/root/Main/World/Escena_" + str(stage) + "/P1")
	countPropaganda = get_node("/root/Main/World/Escena_" + str(stage) + "/Propaganda").get_child_count()
	$Words/Counter.visible = true
	$Words/Counter.text = str(countPropaganda)
	$Words/Counter.modulate = Color("#ffffff")

func youAreDeadSon():
	player.alive = false
	#Todo: play sound and restart game

func changePropaganda():
	countPropaganda -= 1
	if (countPropaganda == 1):
		$Words/Counter.modulate = Color("#00ae1b")
	$Words/Counter.text = str(countPropaganda)
	if (countPropaganda == 0):
		changeStage()
