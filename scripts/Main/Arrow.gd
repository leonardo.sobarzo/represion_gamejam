extends Sprite

onready var Menu = get_node("/root/Main/Words/Menu")
var position_start: bool
var pos1: Vector2
var pos2: Vector2

func _ready():
	#Just to save a some bytes, why?... idk
	position_start = true
	pos1 = Vector2(420, 336)
	pos2 = Vector2(420, 420) #HA HA! 420 funny joke... ffs


func _physics_process(_delta):
	if (position_start):
		
		if (Input.is_action_pressed("ui_down")):
			position_start = false
			position = pos2
			
	else:
		if (Input.is_action_pressed("ui_up")):
			position_start = true
			position = pos1
	
	if (Input.is_action_just_pressed("ui_accept")):
		if (position_start):
			owner.changeStage()
			Menu.visible = false
			set_physics_process(false	)
		else:
			get_tree().quit()
			
func reactivate():
	#some fade in or out
	set_physics_process(true)
	Menu.visible = true
