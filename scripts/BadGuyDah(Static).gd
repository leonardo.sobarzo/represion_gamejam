extends Sprite3D

#This is pretty easy and really nice, thanks godot!
enum Enum { Static, Timed_90, Timed_45 }
export(Enum) var behavior
export var rotation_right: bool
export var time_behaviour: float
export var percentBlinkingTime: float

var main: Node
var timer: float
var degreeTotal: float
var complete: bool
var stoped: bool
var seeingArea = Area

func _ready():
	timer = 0
	main = get_node("/root/Main")
	complete = false
	stoped = false
	seeingArea = get_child(2) #Hardcoded, don't move it


func _physics_process(delta):
	movement(delta)

func movement(delta):
	match behavior:
		Enum.Static:
			blink(delta)
		Enum.Timed_45:
			rotateGuy(45, delta)
		Enum.Timed_90:
			rotateGuy(90, delta)

func blink(delta):
	#lots of warnings
	while(timer < time_behaviour):
		timer += delta
		return
	
	if (get_child_count() > 2):
		$SpotLight.visible = false
		remove_child(seeingArea)
		
	while(timer < time_behaviour + time_behaviour * percentBlinkingTime):
		timer += delta
		return
		
	timer = 0
	$SpotLight.visible = true
	add_child(seeingArea)

func rotateGuy(degree: int, delta: float):
	#why I'm like this?
	while(stoped and timer < time_behaviour):
		timer += delta
		return
		
	degreeTotal += degree * delta
	var direction = 1 if rotation_right else -1
	
	stoped = false
	timer = 0
	var helper = direction * degree * delta
	
	if (not complete):
		if (degreeTotal <= degree):
			rotation_degrees = Vector3(0, rotation_degrees.y + helper, 0)
		else:
			#change direction
			complete = true
			stoped = true
			degreeTotal = 0
			return
	else:
		if (degreeTotal <= degree):
			rotation_degrees = Vector3(0, rotation_degrees.y + helper * -1, 0)
		else:
			#change direction
			complete = false
			stoped = true
			degreeTotal = 0
			return
	
func _on_Area_area_entered(area):
	#I don't know if this is slow or fast, I don't know who this works
	#but it works, and that is all that matters
	if (area.get_parent() in get_tree().get_nodes_in_group("Players")):
		main.youAreDeadSon()
