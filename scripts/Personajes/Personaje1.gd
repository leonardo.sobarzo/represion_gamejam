extends KinematicBody

const MOVE_SPEED = 10
const TURN_SPEED = 3.5
var moveDir: int
var turnDir: int
var velocity: float
var alive: bool

func _ready():
	velocity = 0
	turnDir = 0
	moveDir = 0
	alive = true

func _process(_delta):
	#Miziziziz's code, really good start
	#https://www.youtube.com/watch?time_continue=23&v=W5muCvijlsI&feature=emb_title
	
	moveDir = 0
	turnDir = 0
	
	if (Input.is_action_just_pressed("ui_cancel")):
		get_tree().quit()
	
	if Input.is_action_pressed("ui_up"):
		moveDir -= 1
	if Input.is_action_pressed("ui_down"):
		moveDir += 1
	if Input.is_action_pressed("ui_right"):
		turnDir -= 1
	if Input.is_action_pressed("ui_left"):
		turnDir += 1
	
func _physics_process(_delta):
	#Change rotation
	if (alive):
		var help = rotation_degrees.y + (turnDir * TURN_SPEED)
		rotation_degrees = Vector3(rotation_degrees.x, help, rotation_degrees.z)
		
		#Moving character
		var direction = Vector3(sin(deg2rad(rotation_degrees.y)), 0, cos(deg2rad(rotation_degrees.y)))
		move_and_slide(direction * MOVE_SPEED * moveDir, Vector3(0, 1, 0))
