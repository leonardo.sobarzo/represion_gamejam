extends Area

var active: bool
var main: Node

func _ready():
	active = false
	main = get_node("/root/Main")

func _physics_process(_delta):
	if (Input.is_action_just_pressed("ui_select") and active):
		#get rid of signals... I think
		self.disconnect("area_entered", self, "_on_Area_area_entered")
		self.disconnect("area_exited", self, "_on_Area_area_exited")
		get_parent().visible = false
		owner.get_child(1).visible = true
		main.changePropaganda()
		set_physics_process(false)

func _on_Area_area_entered(_area):
	active = true
	

func _on_Area_area_exited(_area):
	active = false
